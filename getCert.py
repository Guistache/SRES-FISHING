import certstream
import socket
from geoip import geolite2 as geo
import requests
from bs4 import BeautifulSoup as BS
from Levenshtein import distance
import os
liste = []


    

class Certificat:

	def __init__(self):
		self.url = ""
		self.domain = ""
		self.adresse_ip = ""
		self.suspect = 0

	def domaineNeedVerif(self):
		if "data" in self.domain or "open" in self.domain or "bigdata" in self.domain or "smartcity" in self.domain:
			return True
		else:
			return False

	def verifIP(self):
		match = geo.lookup(self.adresse_ip)
		if match.country == "RU" or match.country == "CN" or match.country == "SU" :
			self.suspect +=  7
		print (match.country)

	def virusTotal(self):
		apiKey = "546e1895fa48641541a9130b56c94754571b4c1aa0a57c843d4e9a800de3738e"
		params = {'apikey':apiKey ,'url':self.domain}
		reponse = requests.post('http://'+self.domain,data=params)
		#print(reponse.json())

	def estSuspect(self):
		if self.suspect == 0 :
			print ("Certificat non suspect")
			return False
		elif self.suspect < 3 :
			print ("Certificat peu suspect")
			return True
		elif self.suspect < 7 :
			print ("Certificat suspect")
			return True
		else :
			print ("Certificat tres suspect")
			return True

	def CheckURL(self):
		mon_fichier = open("Top500000.txt", "r")
		contenue = mon_fichier.read()
		contenue2 = contenue.split("\n")
		dist = 0
		ack = 100
		ack_ = ""
    		for i in (contenue2):
        		dist = distance(str(self.domain), str(i))
        		if dist < ack:
            			ack = dist 
            			ack_ = i 
        		if dist == 0:
            			break
    		if ack == 0:
        		self.suspect+= 0
    		if ack < 5:
        		self.suspect+= 7
    		if ack < 10:
        		self.suspect+= 2
    		else:
        		self.suspect+= 0

def getListeSiteOpenData():
	fichier  = open('liste.html', "r")
	soup = BS(fichier,"lxml")
	#dictionnaire qui contiendra les paires balise : nombre d'occurences
	dict = {}
	#boucle for pour compter les balises
	for a in soup.find_all('a', href=True):
    		liste.append(a['href'])
		
def callback(message,context):

	if message['message_type'] == "certificate_update":
		cert = Certificat()

		for domain in message['data']['leaf_cert']['all_domains']:
			cert.domain = domain.strip('http://')

		url = message['data']['source']['url']
		cert.url = url
		if cert.domaineNeedVerif() :
			print("\n")
			print ("url = "+cert.url)
			print ("Domaine = "+cert.domain)
			try :
				cert.adresse_ip = socket.gethostbyname(cert.domain)
				print ('ip = '+cert.adresse_ip)
				cert.verifIP()
			except socket.error as msg :
				cert.adresse_ip = "Erreur de resolution de DNS"
			cert.CheckURL()
			cert.estSuspect()
			#cert.virusTotal()
getListeSiteOpenData()	
certstream.listen_for_events(callback)
